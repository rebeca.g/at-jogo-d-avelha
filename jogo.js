var jogador = 0;
var jogadas = 0;
var venceu = false;

function finalizar(){
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function TenteDeNovo() {
	jogadas = 0;
	let next;
	let casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	if(jogador == 0) {
		jogador = 1;
	} else {
		jogador = 0;
	}

	venceu = false;
	document.getElementById('tente-de-novo').style.display = 'none';
	document.getElementById('proximo').innerHTML = 'Próxima Jogada: <span id="vez"></span>'
	document.getElementById("vez").innerHTML = jogador == 0 ? "Piu Piu" : "Frajola"
};

function clique(id) {
	console.log("jogador: "+jogador);

	var tag = null;

	if(id.currentTarget)
		tag = id.currentTarget;
	else
		tag = document.getElementById('casa'+id);

	if((tag.style.backgroundImage == '' || tag.style.backgroundImage == null) && venceu === false)
	{
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		var ganhou = 0
		if(jogadas >= 5){
			var ganhou = verificarGanhador(tag);

			switch(ganhou)
			{
				case 1:
						let next = jogador == 0 ? 'Piu Piu' : 'Frajola';
						venceu = true;
						document.getElementById('tente-de-novo').style.display = 'block';
						document.getElementById("proximo").innerHTML = `Vencedor: <span class="${next}" id="vez">${next}</span>`
						return;
					break;
				case -1:
					venceu = true;
					document.getElementById('tente-de-novo').style.display = 'block';
					document.getElementById("proximo").innerHTML = "Empate!"
					break;
				case 0:
					break;
			};
		};

		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			jogador = 1;
			vez.className = 'Frajola'
			vez.innerHTML = 'Frajola';
		}else
		{
			jogador = 0;
			vez.className = 'Piu Piu'
			vez.innerHTML = 'Piu Piu';
		}

		if(ganhou!=0)
		{
			finalizar();
		}
	}
}

function verificarGanhador(jogada){
	const possibilidades = {
    'casa1': [[1, 2, 3], [1, 5, 9], [1, 4, 7]],
    'casa2': [[1, 2, 3], [2, 5, 8]],
    'casa3': [[1, 2, 3], [7, 5, 3], [3, 6, 9]],
    'casa4': [[4, 5, 6], [1, 4, 7]],
    'casa5': [[4, 5, 6], [1, 5, 9], [3, 5, 7], [2, 5, 8]],
    'casa6': [[4, 5, 6], [3, 6, 9]],
    'casa7': [[7, 8, 9], [3, 5, 7], [1, 4, 7]],
    'casa8': [[7, 8, 9], [2, 5, 8]],
    'casa9': [[7, 8, 9], [1, 5, 9], [3, 6, 9]],
  };
	let casa = jogada.id;
	var possibilidade = possibilidades[casa];

	for( let pos = 0; pos < possibilidade.length; pos++ ){
		let [casaA, casaB, casaC] = possibilidade[pos];
		let c1 = document.getElementById(`casa${casaA}`).style.backgroundImage;
		let c2 = document.getElementById(`casa${casaB}`).style.backgroundImage;
		let c3 = document.getElementById(`casa${casaC}`).style.backgroundImage;

		if( c1 == c2 && c1 == c3 && c1 != '' ) {
			return 1;
		};
	};

	if( jogadas == 9) {
		return -1;
	};

	return 0;
}
